document.addEventListener('DOMContentLoaded', function() {
    var form = document.getElementById('form');

    form.addEventListener('submit', function(event) {
        event.preventDefault();

        var nama = document.getElementById('nama').value;
        var email = document.getElementById('email').value;
        var tlp = document.getElementById('tlp').value;
        var alamat = document.getElementById('alamat').value;
        var tglahir = document.getElementById('tglahir').value;

        if (!nama || !email || !tlp || !alamat || !tglahir) {
            alert("Silakan lengkapi semua data sebelum mengirimkan formulir.");
        } else {
            var alertText = "Data yang Diinput:\n";
            alertText += "Nama: " + nama + "\n";
            alertText += "Email: " + email + "\n";
            alertText += "No. Telepon: " + tlp + "\n";
            alertText += "Alamat: " + alamat + "\n";
            alertText += "Tanggal Lahir: " + tglahir;

            var resetConfirmation = confirm("Anda yakin ingin mengirimkan formulir?");
            if (resetConfirmation) {
                form.reset();
            }
        }

        alert(alertText);

    });

    form.addEventListener('reset', function(event) {

        var nama = document.getElementById('nama').value;
        var email = document.getElementById('email').value;
        var tlp = document.getElementById('tlp').value;
        var alamat = document.getElementById('alamat').value;
        var tglahir = document.getElementById('tglahir').value;

        if (!nama || !email || !tlp || !alamat || !tglahir) {
            alert("Silakan lengkapi semua data sebelum mengirimkan formulir.");
        } else {
            var alertText = "Data yang Diinput:\n";
            alertText += "Nama: " + nama + "\n";
            alertText += "Email: " + email + "\n";
            alertText += "No. Telepon: " + tlp + "\n";
            alertText += "Alamat: " + alamat + "\n";
            alertText += "Tanggal Lahir: " + tglahir;

            var resetConfirmation = confirm("Anda yakin ingin mengirimkan formulir?");
            if (resetConfirmation) {
                form.reset();
            }
        }

    });

});
